# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_file = tplroot ~ '.config.file' %}
{%- from tplroot ~ "/map.jinja" import mounts with context %}

include:
  - {{ sls_config_file }}

mounts-service-running-autofs-service-running:
  service.running:
    - name: {{ mounts.service.autofs.name }}
    - enable: True
    - watch:
      - sls: {{ sls_config_file }}
