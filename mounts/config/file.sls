# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- set sls_service_running = tplroot ~ '.service.running' %}
{%- from tplroot ~ "/map.jinja" import mounts with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}
  - {{ sls_service_running }}

{%- set bindmounts = mounts.get('bind', {}) %}
{%- set directorymounts = mounts.get('directories', {}) %}
{%- set absentmounts = mounts.get('absent', {}) %}
{%- set symlinkmounts = mounts.get('symlinks', {}) %}
{%- set autofsmounts = mounts.get('autofs', {}) %}

{%- for bind, data in bindmounts.items() %}
  {%- if data.type is defined %}
    {%- set type = data.type %}
  {%- else %}
    {%- set type = 'user' %}
  {%- endif %}
  {%- if data.createtarget is defined %}
    {%- set createtarget = data.createtarget %}
  {%- else %}
    {%- set createtarget = True %}
  {%- endif %}

  {%- if type == 'user' %}
    {%- for username, user in salt.pillar.get('users', {}).items() if (username != 'root' and (user.absent is not defined or not user.absent)) %}
      {%- set name = data.name|replace('username', username) %}
      {%- set target = data.target|replace('username', username) %}
      {%- if data.user is defined %}
        {%- set user = data.user|replace('username', username) %}
      {%- else %}
        {%- set user = username %}
      {%- endif %}
      {%- if data.group is defined %}
        {%- set group = data.group|replace('username', username) %}
      {%- else %}
        {%- set group = username %}
      {%- endif %}
      {%- if data.dir_mode is defined %}
        {%- set dir_mode = data.dir_mode %}
      {%- else %}
        {%- set dir_mode = '0755' %}
      {%- endif %}
mounts-config-file-bind-mounts-{{ bind }}-{{ username }}-mount-point-exists:
  file.directory:
    - name: {{ name }}
    - user: {{ user }}
    - group: {{ group }}
    - makedirs: True
    - dir_mode: {{ dir_mode }}
    - require:
      - user: {{ user }}
      - group: {{ group }}

{%- if createtarget %}
mounts-config-file-bind-mounts-{{ bind }}-{{ username }}-target-point-exists:
  file.directory:
    - name: {{ target }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: {{ dir_mode }}
    - require:
      - user: {{ user }}
      - group: {{ group }}
{%- endif %}


mounts-config-file-bind-mounts-{{ bind }}-{{ username }}-mounted:
  mount.mounted:
    - name: {{ name }}
    - device: {{ target }}
    - fstype: none
    - opts: bind
    - dump: 0
    - pass_num: 0
    - persist: True
    - mkmnt: False
    - require:
      - mounts-config-file-bind-mounts-{{ bind }}-{{ username }}-mount-point-exists
      {%- if createtarget %}
      - mounts-config-file-bind-mounts-{{ bind }}-{{ username }}-target-point-exists
      {%- endif %}
{%- endfor %}
{%- else %}
{%- set name = data.name %}
{%- if data.user is defined %}
{%- set user = data.user %}
{%- else %}
{%- set user = 'root' %}
{%- endif %}
{%- if data.group is defined %}
{%- set group = data.group %}
{%- else %}
{%- set group = 'root' %}
{%- endif %}
{%- if data.dir_mode is defined %}
{%- set dir_mode = data.dir_mode %}
{%- else %}
{%- set dir_mode = '0755' %}
{%- endif %}
mounts-config-file-bind-mounts-{{ bind }}-mount-point-exists:
  file.directory:
    - name: {{ name }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: {{ dir_mode }}
    - require:
      - user: {{ user }}
      - group: {{ group }}

{%- if createtarget %}
mounts-config-file-bind-mounts-{{ bind }}-target-point-exists:
  file.directory:
    - name: {{ target }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: {{ dir_mode }}
    - require:
      - user: {{ user }}
      - group: {{ group }}
{%- endif %}

mounts-config-file-bind-mounts-{{ bind }}-mounted:
  mount.mounted:
    - name: {{ name }}
    - device: {{ target }}
    - fstype: none
    - opts: bind
    - dump: 0
    - pass_num: 0
    - persist: True
    - mkmnt: False
    - require:
      - mounts-config-file-bind-mounts-{{ bind }}-mount-point-exists
      {%- if createtarget %}
      - mounts-config-file-bind-mounts-{{ bind }}-target-point-exists
      {%- endif %}
{%- endif %}
{%- endfor %}

{%- for directory, data in directorymounts.items() %}
  {%- if data.type is defined %}
    {%- set type = data.type %}
  {%- else %}
    {%- set type = 'user' %}
  {%- endif %}

  {%- if type == 'user' %}
    {%- for username, user in salt.pillar.get('users', {}).items() if (username != 'root' and (user.absent is not defined or not user.absent)) %}
      {%- set name = data.name|replace('username', username) %}
      {%- if data.user is defined %}
        {%- set user = data.user|replace('username', username) %}
      {%- else %}
        {%- set user = username %}
      {%- endif %}
      {%- if data.group is defined %}
        {%- set group = data.group|replace('username', username) %}
      {%- else %}
        {%- set group = username %}
      {%- endif %}
      {%- if data.dir_mode is defined %}
        {%- set dir_mode = data.dir_mode %}
      {%- else %}
        {%- set dir_mode = '0755' %}
      {%- endif %}
mount-config-file-directory-mounts-{{ directory }}-{{ username }}-managed:
  file.directory:
    - name: {{ name }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: {{ dir_mode }}
    - require:
      - user: {{ user }}
      - group: {{ group }}

    {%- endfor %}
  {%- else %}
    {%- set name = data.name %}
    {%- if data.user is defined %}
      {%- set user = data.user %}
    {%- else %}
      {%- set user = 'root' %}
    {%- endif %}
    {%- if data.group is defined %}
      {%- set group = data.group %}
    {%- else %}
      {%- set group = 'root' %}
    {%- endif %}
    {%- if data.dir_mode is defined %}
      {%- set dir_mode = data.dir_mode %}
    {%- else %}
      {%- set dir_mode = '0755' %}
    {%- endif %}
mount-config-file-directory-mounts-{{ directory }}-managed:
  file.directory:
    - name: {{ name }}
    - user: {{ user }}
    - group: {{ group }}
    - dir_mode: {{ dir_mode }}
    - require:
      - user: {{ user }}
      - group: {{ group }}
  {%- endif %}
{%- endfor %}

{%- for absent, data in absentmounts.items() %}
  {%- if data.type is defined %}
    {%- set type = data.type %}
  {%- else %}
    {%- set type = 'user' %}
  {%- endif %}

  {%- if type == 'user' %}
    {%- for username, user in salt.pillar.get('users', {}).items() if (username != 'root' and (user.absent is not defined or not user.absent)) %}
      {%- set name = data.name|replace('username', username) %}
mount-config-file-absent-mounts-{{ absent }}-{{ username }}-not-present:
  file.absent:
    - name: {{ name }}
    {%- endfor %}
  {%- else %}
mount-config-file-absent-mounts-{{ absent }}-not-present:
  file.absent:
    - name: {{ name }}
  {%- endif %}
{%- endfor %}

{%- for symlink, data in symlinkmounts.items() %}
  {%- if data.type is defined %}
    {%- set type = data.type %}
  {%- else %}
    {%- set type = 'user' %}
  {%- endif %}

  {%- if type == 'user' %}
    {%- for username, user in salt.pillar.get('users', {}).items() if (username != 'root' and (user.absent is not defined or not user.absent)) %}
      {%- set name = data.name|replace('username', username) %}
      {%- set target = data.target|replace('username', username) %}
      {%- if data.user is defined %}
        {%- set user = data.user|replace('username', username) %}
      {%- else %}
        {%- set user = username %}
      {%- endif %}
      {%- if data.group is defined %}
        {%- set group = data.group|replace('username', username) %}
      {%- else %}
        {%- set group = username %}
      {%- endif %}
      {%- if data.force is defined %}
        {%- set force = data.force %}
      {%- else %}
        {%- set force = False %}
      {%- endif %}
mounts-config-file-symlink-mounts-{{ symlink }}-{{ username }}-symlink-exists:
  file.symlink:
    - name: {{ name }}
    - target: {{ target }}
    - force: {{ force }}
    - user: {{ user }}
    - group: {{ group }}
    - require:
      - user: {{ user }}
      - group: {{ group }}
    {%- endfor %}
  {%- else %}
    {%- set name = data.name %}
    {%- set target = data.target %}
    {%- if data.user is defined %}
      {%- set user = data.user %}
    {%- else %}
      {%- set user = 'root' %}
    {%- endif %}
    {%- if data.group is defined %}
      {%- set group = data.group|replace('username', username) %}
    {%- else %}
      {%- set group = 'root' %}
    {%- endif %}
    {%- if data.force is defined %}
      {%- set force = data.force %}
    {%- else %}
      {%- set force = False %}
    {%- endif %}
mounts-config-file-symlink-mounts-{{ symlink }}-symlink-exists:
  file.symlink:
    - name: {{ name }}
    - target: {{ target }}
    - force: {{ force }}
    - user: {{ user }}
    - group: {{ group }}
    - require:
      - user: {{ user }}
      - group: {{ group }}
  {%- endif %}
{%- endfor %}

{%- for autofs, data in autofsmounts.items() %}
  {%- if data.name is defined %}
  {%- set name = data.name %}
  {%- else %}
  {%- set name = autofs %}
  {%- endif %}
  {%- if data.mountpoint is defined %}
  {%- set mountpoint = data.mountpoint %}
  {%- else %}
  {%- set mountpoint = '/mnt' %}
  {%- endif %}
  {%- set server = data.server %}
  {%- set share_location = data.share_location %}
  {%- if data.timeout is defined %}
  {%- set timeout = data.timeout %}
  {%- else %}
  {%- set timeout = 60 %}
  {%- endif %}
  {%- if mount_options is defined %}
  {%- set mount_options = data.mount_options %}
  {%- else %}
  {%- set mount_options = '-rw,soft,intr,rsize=8192,wsize=8192' %}
  {%- endif %}
mounts-config-file-autofs-{{ server }}-{{ name }}-{{ mountpoint }}-mounts-added-to-master-conf:
  file.append:
    - name: {{ mounts.config.autofs.conf_location ~ 'master' }}
    - text: {{ mountpoint }} {{ mounts.config.autofs.conf_location ~ server }} --timeout {{ timeout }}
    - require:
      - sls: {{ sls_package_install }}

mounts-config-file-autofs-config-{{ server }}-{{ name }}-added:
  file.append:
    - name: {{ mounts.config.autofs.conf_location ~ server }}
    - text: {{ name }} {{ mount_options }} {{ server }}:{{ share_location }}
    - require:
      - sls: {{ sls_package_install }}
    {%- if loop.last %}
    - watch_in: 
      - sls: {{ sls_service_running }}
    {%- endif %}
{%- endfor %}

{%- if mounts.debug %}
mounts-config-file-mounts-mounts-test-file-managed:
  file.managed:
    - name: /tmp/mountsdata
    - contents: {{ mounts }}

mounts-config-file-bind-mounts-test-file-managed:
  file.managed:
    - name: /tmp/binddata
    - contents: {{ bindmounts }}

mounts-config-file-directory-mounts-test-file-managed:
  file.managed:
    - name: /tmp/directorydata
    - contents: {{ directorymounts }}

mounts-config-file-absent-mounts-test-file-managed:
  file.managed:
    - name: /tmp/absentdata
    - contents: {{ absentmounts }}

mounts-config-file-symlink-mounts-test-file-managed:
  file.managed:
    - name: /tmp/symlinkdata
    - contents: {{ symlinkmounts }}

mounts-config-file-pillar-mounts-test-file-managed:
  file.managed:
    - name: /tmp/pillardata
    - contents_pillar: mounts

{%- set tempdata = salt['pillar.get']('mounts') %}
mounts-config-file-pillar-direct-mounts-test-file-managed:
  file.managed:
    - name: /tmp/pillardirectdata
    - contents: {{ tempdata }}
{%- endif %}
