.. _readme:

mounts-formula
================

|img_travis| |img_sr|

.. |img_travis| image:: https://travis-ci.com/saltstack-formulas/mounts-formula.svg?branch=master
   :alt: Travis CI Build Status
   :scale: 100%
   :target: https://travis-ci.com/saltstack-formulas/mounts-formula
.. |img_sr| image:: https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg
   :alt: Semantic Release
   :scale: 100%
   :target: https://github.com/semantic-release/semantic-release

A SaltStack formula for managing user and system directories.

.. contents:: **Table of Contents**
   :depth: 1

General notes
-------------
There are many dependencies with managing directories, so I've decided to put
them all in the same spot.

This is ordered into four groups:
- bind mounts
- directories
- absent
- symlinks
- autofs

Bind mounts are mounted filesystems from one directory to another.

Directories are managed directories.

Absent are directories that should *not* exist on the filesystem

Symlinks are managed symlinks

Autofs rules are purely for NFS autodiscovery. See
https://wiki.archlinux.org/index.php/Autofs#NFS_network_mounts

Each group has two types: user and system. A system entry will be created
once, whereas a user entry will be created for each user.

The user type is default.

All pillar data for user entries will be run through a parser, which will
replace the string "username" with the correct user.

The owner will default to the current user.
The group will default the the current user.
createtarget will default to True
force will default to False

Autofs mounts do not support user types

Pillar data should look like this::

   mounts:
     bind:
       videos:
         name: /home/username/videos
         target: /srv/videos/username
         user: username
         group: users
         createtarget: True  # If the target directory needs to be created as well
         type: user
     directories:
       backup:
         name: /home/username/backup
       documents:
         name: /home/username/documents
         permissions: 2755
     absent:
       Rubbish:
         name: /home/username/Rubbish
     symlinks:
       Downloads:
         name: /home/username/Downloads
         target: /home/username/downloads
         force: True
     autofs:
       mnt:
         name: /mnt
         timeout: 60  # Defaults to 60

See the full `SaltStack Formulas installation and usage instructions
<https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

If you are interested in writing or contributing to formulas, please pay attention to the `Writing Formula Section
<https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html#writing-formulas>`_.

If you want to use this formula, please pay attention to the ``FORMULA`` file and/or ``git tag``,
which contains the currently released version. This formula is versioned according to `Semantic Versioning <http://semver.org/>`_.

See `Formula Versioning Section <https://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html#versioning>`_ for more details.

If you need (non-default) configuration, please pay attention to the ``pillar.example`` file and/or `Special notes`_ section.

Contributing to this repo
-------------------------

**Commit message formatting is significant!!**

Please see `How to contribute <https://github.com/saltstack-formulas/.github/blob/master/CONTRIBUTING.rst>`_ for more details.

Special notes
-------------

None

Available states
----------------

.. contents::
   :local:

``mounts``
^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This installs the mounts package,
manages the mounts configuration file and then
starts the associated mounts service.

``mounts.package``
^^^^^^^^^^^^^^^^^^^^

This state will install the mounts package only.

``mounts.config``
^^^^^^^^^^^^^^^^^^^

This state will configure the mounts service and has a dependency on ``mounts.install``
via include list.

``mounts.service``
^^^^^^^^^^^^^^^^^^^^

This state will start the mounts service and has a dependency on ``mounts.config``
via include list.

``mounts.clean``
^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

this state will undo everything performed in the ``mounts`` meta-state in reverse order, i.e.
stops the service,
removes the configuration file and
then uninstalls the package.

``mounts.service.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will stop the mounts service and disable it at boot time.

``mounts.config.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^

This state will remove the configuration of the mounts service and has a
dependency on ``mounts.service.clean`` via include list.

``mounts.package.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will remove the mounts package and has a depency on
``mounts.config.clean`` via include list.

``mounts.subcomponent``
^^^^^^^^^^^^^^^^^^^^^^^^^

*Meta-state (This is a state that includes other states)*.

This state installs a subcomponent configuration file before
configuring and starting the mounts service.

``mounts.subcomponent.config``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will configure the mounts subcomponent and has a
dependency on ``mounts.config`` via include list.

``mounts.subcomponent.config.clean``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This state will remove the configuration of the mounts subcomponent
and reload the mounts service by a dependency on
``mounts.service.running`` via include list and ``watch_in``
requisite.

Testing
-------

Linux testing is done with ``kitchen-salt``.

Requirements
^^^^^^^^^^^^

* Ruby
* Docker

.. code-block:: bash

   $ gem install bundler
   $ bundle install
   $ bin/kitchen test [platform]

Where ``[platform]`` is the platform name defined in ``kitchen.yml``,
e.g. ``debian-9-2019-2-py3``.

``bin/kitchen converge``
^^^^^^^^^^^^^^^^^^^^^^^^

Creates the docker instance and runs the ``mounts`` main state, ready for testing.

``bin/kitchen verify``
^^^^^^^^^^^^^^^^^^^^^^

Runs the ``inspec`` tests on the actual instance.

``bin/kitchen destroy``
^^^^^^^^^^^^^^^^^^^^^^^

Removes the docker instance.

``bin/kitchen test``
^^^^^^^^^^^^^^^^^^^^

Runs all of the stages above in one go: i.e. ``destroy`` + ``converge`` + ``verify`` + ``destroy``.

``bin/kitchen login``
^^^^^^^^^^^^^^^^^^^^^

Gives you SSH access to the instance for manual testing.

