# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mounts with context %}

mounts-package-install-nfs-pkg-installed:
  pkg.installed:
    - pkgs: {{ mounts.pkgs.nfs.pkgs | yaml }}

mounts-package-install-autofs-pkg-installed:
  pkg.installed:
    - pkgs: {{ mounts.pkgs.autofs.pkgs | yaml }}
