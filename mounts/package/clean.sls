# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mounts with context %}

mounts-package-clean-nfs-pkg-removed:
  pkg.removed:
    - pkgs: {{ mounts.pkgs.nfs.pkgs }}

mounts-package-clean-autofs-pkg-removed:
  pkg.removed:
    - pkgs: {{ mounts.pkgs.autofs.pkgs }}
