# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mounts with context %}

mounts-service-clean-autofs-service-dead:
  service.dead:
    - name: {{ mounts.service.autofs.name }}
    - enable: False
